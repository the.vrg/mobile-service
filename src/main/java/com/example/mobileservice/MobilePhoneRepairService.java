package com.example.mobileservice;

import com.example.mobileservice.supplier.MobilePartSupplier;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vrg
 */
public class MobilePhoneRepairService {

    private HashMap<MobilePartType, Integer> stock = new HashMap<>();
    private List<WorkSheet>worksheetsToProcess = new ArrayList<>();
    private Map<MobilePartType,Long>orderIdByPartType = new HashMap<>();
    private MobilePartSupplier mobilePartSupplier;

    public MobilePhoneRepairService(MobilePartSupplier mobilePartSupplier) {
        this.mobilePartSupplier = mobilePartSupplier;
    }
    
    public synchronized WorkSheet sendIn(Mobile mobile) {
        WorkSheet worksheet = createWorkSheet(mobile);
        collectFailingParts(worksheet);
        worksheetsToProcess.add(worksheet);
        return worksheet;
    }

    public synchronized boolean replaceAllPossibleFailingParts(WorkSheet ws) {
        if (ws.partsToReplace == null) {
            //nothing to replace
            ws.status = WorkSheet.Status.FINISHED;
            return true;
        }
        for (Iterator<MobilePart> it = ws.partsToReplace.iterator(); it.hasNext();) {
            MobilePart mobilePartToReplace = it.next();
            MobilePartType type = mobilePartToReplace.type;
            Integer inStockQuantity = stock.get(type);
            if (inStockQuantity != null && inStockQuantity > 0) {
                inStockQuantity = inStockQuantity - 1;
                stock.put(type, inStockQuantity);
                if (ws.replacedParts == null) {
                    ws.replacedParts = new ArrayList<>();
                }
                ws.replacedParts.add(new ReplacedMobilePart(type, mobilePartToReplace.name));
                it.remove();
            } else {
                addOrderForMissingPart(type);
                ws.status = WorkSheet.Status.WAITING_FOR_PARTS;
            }
        }
        
        if (ws.partsToReplace.isEmpty()) {
            ws.status = WorkSheet.Status.FINISHED;
            return true;
        } else {
            return false;
        }
    }
    
    public synchronized void addOrderForMissingPart(MobilePartType type) {
        if (!orderIdByPartType.containsKey(type)) {
            orderIdByPartType.put(type, null);
        }
    }
    
    public synchronized void processWorksheets() {
        
        for (Iterator<WorkSheet> it = worksheetsToProcess.iterator(); it.hasNext();) {
            WorkSheet workSheet = it.next();
            boolean finished = replaceAllPossibleFailingParts(workSheet);
            if (finished) {
                it.remove();
            }
        }
    }

    public synchronized boolean replacePart(WorkSheet ws, MobilePart mobilePart) {
        MobilePartType type = mobilePart.type;
        Integer inStockQuantity = stock.get(type);
        if (inStockQuantity != null && inStockQuantity > 0) {
            inStockQuantity = inStockQuantity - 1;
            stock.put(type, inStockQuantity);
            if (ws.replacedParts == null) {
                ws.replacedParts = new ArrayList<>();
            }
            ws.replacedParts.add(new ReplacedMobilePart(type, mobilePart.name));
            ws.partsToReplace.remove(mobilePart);
            return true;
        } else {
            return false;
        }

    }

    public synchronized WorkSheet createWorkSheet(Mobile mobile) {
        WorkSheet ws = new WorkSheet();
        ws.mobile = mobile;
        ws.receivedOn = new Date();
        ws.status = WorkSheet.Status.RECEIVED;
        return ws;
    }

    public synchronized void collectFailingParts(WorkSheet sheet) {
        
        sheet.status = WorkSheet.Status.STARTED_TO_REPAIR;
        
        Mobile mobile = sheet.mobile;

        if (mobile.display.failing) {
            addPartToReplace(sheet, mobile.display);
        }
        if (mobile.keyboard != null && mobile.keyboard.failing) {
            addPartToReplace(sheet, mobile.keyboard);
        }
        if (mobile.microphone.failing) {
            addPartToReplace(sheet, mobile.microphone);
        }
        if (mobile.motherBoard.failing) {
            addPartToReplace(sheet, mobile.motherBoard);
        }
        if (mobile.powerSwitch.failing) {
            addPartToReplace(sheet, mobile.powerSwitch);
        }
        if (mobile.speaker.failing) {
            addPartToReplace(sheet, mobile.speaker);
        }
        if (mobile.volumeButtons.failing) {
            addPartToReplace(sheet, mobile.volumeButtons);
        }
    }

    public synchronized void addPartToReplace(WorkSheet sheet, MobilePart mobilePart) {
        if (sheet.partsToReplace == null) {
            sheet.partsToReplace = new ArrayList<>();
        }
        sheet.partsToReplace.add(mobilePart);
    }
    
    public synchronized void orderParts() {
        if (!orderIdByPartType.isEmpty()) {
            for (Map.Entry<MobilePartType, Long> entry : orderIdByPartType.entrySet()) {
                Long orderId = entry.getValue();
                if (orderId == null) {
                    orderId = mobilePartSupplier.orderPart(entry.getKey(), 5);
                    entry.setValue(orderId);
                } 
            }
        }
    }
    
    public synchronized void pollSupplier() {
        if (!orderIdByPartType.isEmpty()) {
            List<Long> orderIds = new ArrayList<>(orderIdByPartType.values());
            for (Long orderId : orderIds) {
                if (orderId != null) {
                    if (mobilePartSupplier.isReadyForShipment(orderId)) {
                        Order shippedOrder = mobilePartSupplier.shipOrder(orderId);
                        stock.put(shippedOrder.getType(), shippedOrder.getQuantity());
                        orderIdByPartType.remove(shippedOrder.getType());
                    }
                }
            }
        }
    }
}
